<?php

namespace App\Http\Controllers;

use ATehnix\VkClient\Exceptions\VkException;
use Illuminate\Http\Request;
use App\Models\UserDataFromSocialNetworkFactory;
use ATehnix\VkClient\Auth;
use ATehnix\VkClient\Client;

class VkController extends Controller
{
    public function show(Request $request) {

        $auth = new Auth(env('VKONTAKTE_KEY'), env('VKONTAKTE_SECRET'), env('VKONTAKTE_REDIRECT_URI'));

        if(!isset($request->code)) {
            return redirect($auth->getUrl());
        }

        try {
            $token = $auth->getToken($request->code);
            session(['VK_TOKEN' => $token]);
        } catch (VkException $e) {
            return redirect($auth->getUrl());
        }


        $api = new Client;
        $userDataFromVk = UserDataFromSocialNetworkFactory::prepareUserDataFromVk($token, $api);

        return view('vk', [
            'img' => $userDataFromVk->getMainPhoto(),
            'name' => $userDataFromVk->getName()
        ]);
    }
}
