<?php


namespace App\Models;


interface UserDataFromSocialNetworkInterface
{
    /**
     * @return string
     */
    function getName();

    /**
     * @return string
     */
    function getMainPhoto();

}
