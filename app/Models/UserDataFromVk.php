<?php


namespace App\Models;


use ATehnix\VkClient\Client;


class UserDataFromVk implements UserDataFromSocialNetworkInterface
{

    private $token;

    /** @var Client */
    private $vkApiClient;

    private $name;
    private $mainPhoto;

    public function __construct($token, Client $vkApiClient)
    {
        $this->setToken($token);
        $this->setVkApiClient($vkApiClient);
    }

    /**
     * @inheritDoc
     */
    function getName()
    {
        if(!isset($this->name)) {
            $this->name = $this->getResponse()['first_name'].' '.$this->getResponse()['last_name'];
        }
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    function getMainPhoto()
    {
        if(!isset($this->mainPhoto)) {
            $this->mainPhoto = $this->getResponse()['photo_max'];
        }
        return $this->mainPhoto;
    }

    /**
     * @return mixed
     * @throws \ATehnix\VkClient\Exceptions\VkException
     */
    private function getResponse()
    {
        $result = $this->getVkApiClient()->request('users.get', ['fields' => 'photo_max'], $this->getToken());
        return reset($result['response']);
    }



    /**
     * @return Client
     */
    private function getVkApiClient()
    {
        return $this->vkApiClient;
    }

    /**
     * @param Client $vkApiClient
     */
    private function setVkApiClient(Client $vkApiClient): void
    {
        $this->vkApiClient = $vkApiClient;
    }

    /**
     * @return mixed
     */
    private function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    private function setToken($token): void
    {
        $this->token = $token;
    }
}
