<?php


namespace App\Models;


use ATehnix\VkClient\Client;

class UserDataFromSocialNetworkFactory
{
    /**
     * @param $token
     * @param Client $vkApiClient
     * @return UserDataFromSocialNetworkInterface
     */
    public static function prepareUserDataFromVk($token, Client $vkApiClient):UserDataFromSocialNetworkInterface {
        return new UserDataFromVk($token, $vkApiClient);
    }
}
